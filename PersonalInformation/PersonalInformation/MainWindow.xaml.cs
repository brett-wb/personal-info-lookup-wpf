﻿using System;
using System.Windows;

namespace PersonalInformation
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        VM vm = new VM();
        public MainWindow()
        {
            InitializeComponent();
            DataContext = vm;
        }

        private void BtnAddData_Click(object sender, RoutedEventArgs e)
        {
            vm.AddData();
        }
    }
}
