﻿using Microsoft.Win32;
using System.ComponentModel;
using System.IO;
using System.Runtime.CompilerServices;

namespace PersonalInformation
{
    class VM : INotifyPropertyChanged
    {
        const string WRONG_TYPE = "Please select a valid text file.";
        const string WRONG_FORMAT = "Make sure your text file is in the right format.";
        public BindingList<PersonalData> PersonalDatas { get; set; } = new BindingList<PersonalData>();
        private string errorMessage;
        public string ErrorMessage
        {
            get { return errorMessage; }
            set { errorMessage = value; NotifyChange(); }
        }

        public VM()
        {
            PersonalDatas.Add(new PersonalData() { Name = "Brett Behring", Address = "4583 Acton Avenue", Age = "23", PhoneNum = "519-497-5347" });
            PersonalDatas.Add(new PersonalData() { Name = "Autumn Blair", Address = "3746 James Street", Age = "22", PhoneNum = "226-479-1533" });
            PersonalDatas.Add(new PersonalData() { Name = "Max Williams", Address = "640 Heavner Avenue", Age = "25", PhoneNum = "416-294-3803" });
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyChange([CallerMemberName] string property = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(property));
        }

        public void AddData()
        {
            ErrorMessage = string.Empty;
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
            if (openFileDialog.ShowDialog() == true
                && Path.GetExtension(openFileDialog.FileName) == ".txt")
            {
                string[] data = File.ReadAllLines(openFileDialog.FileName);
                if (data.Length == 4)
                    PersonalDatas.Add(new PersonalData() { Name = data[0], Address = data[1], Age = data[2], PhoneNum = data[3] });
                else
                    ErrorMessage = WRONG_FORMAT;
            }
            else
                ErrorMessage = WRONG_TYPE;
        }
    }

    class PersonalData
    {
        public string Name { get; set; }
        public string Address { get; set; }
        public string Age { get; set; }
        public string PhoneNum { get; set; }
    }
}
