# Personal Info Lookup (WPF)

Copyright (c) 2019 Brett Behring, released under the [MIT license](https://opensource.org/licenses/MIT).  
I chose this license because I don't mind if anyone uses or modifies this software, so long as they also hold this same mentality with their version.

This is an application that allows the user to browse personal data from a text file in a more convenient format.
___

# How to Build
1. Download Visual Studio along with the files in this repository.
2. Open the .sln project file in the PersonalInformation folder in Visual Studio.
3. Click the "Start" button near the top of the window to build + execute.